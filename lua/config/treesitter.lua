local M = {}

function M.setup()

  require('nvim-treesitter.configs').setup {

    -- general
    auto_install = true,

    matchup = {
      enable = true,
      include_match_words = true
    },

    highlight = {
      enable = true,

      -- Do not enable highlight for file larger than 100 KB
      -- A snippet from treesitter doc
      disable = function(_, buf)
        local max_filesize = 100 * 1024 -- 100 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
      end,
    },

    -- nvim-treesitter-textobjects

    textobjects = {
      select = {
        enable = true,

        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = true,

        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ["af"] = { query = "@function.outer", desc = "Select inner part of function region"},
          ["if"] = "@function.inner",
          ["ac"] = "@class.outer",
          ["ic"] = "@class.inner",
        },
      },

      swap = {
        enable = true,
        swap_next = {
          ["<leader>rx"] = "@parameter.inner",
        },
        swap_previous = {
          ["<leader>rX"] = "@parameter.inner",
        },
      },

      move = {
        enable = true,

        set_jumps = true, -- whether to set jumps in the jumplist, which we want
        goto_next_start = {
          ["]m"] = "@function.outer",
          ["]]"] = "@class.outer",
        },

        goto_next_end = {
          ["]M"] = "@function.outer",
          ["]["] = "@class.outer",
        },

        goto_previous_start = {
          ["[m"] = "@function.outer",
          ["[["] = "@class.outer",
        },

        goto_previous_end = {
          ["[M"] = "@function.outer",
          ["[]"] = "@class.outer",
        },
      },

      lsp_interop = {
        enable = true,
        border = "none",
        peek_definition_code = {
          ["<leader>df"] = "@function.outer",
          ["<leader>dF"] = "@class.outer",
        },
      },
    }
  }
end

return M
