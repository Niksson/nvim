-- This is configuration for 'alpha' - the startup screen
-- https://github.com/goolord/alpha-nvim
local M = {}

function M.setup()
	-- Checks if we can import 'alpha' module
	-- if not - return immediately
  local status_ok, alpha = pcall(require, "alpha")
  if not status_ok then
    return
  end

	-- Use 'dashboard' theme of 'alpha
  local dashboard = require "alpha.themes.dashboard"
  local function header()
    return {
      [[                                           bbbbbbbb            ]],
      [[                                           b::::::b            ]],
      [[                                           b::::::b            ]],
      [[                                           b::::::b            ]],
      [[                                            b:::::b            ]],
      [[nnnn  nnnnnnnn    vvvvvvv           vvvvvvv b:::::bbbbbbbbb    ]],
      [[n:::nn::::::::nn   v:::::v         v:::::v  b::::::::::::::bb  ]],
      [[n::::::::::::::nn   v:::::v       v:::::v   b::::::::::::::::b ]],
      [[nn:::::::::::::::n   v:::::v     v:::::v    b:::::bbbbb:::::::b]],
      [[  n:::::nnnn:::::n    v:::::v   v:::::v     b:::::b    b::::::b]],
      [[  n::::n    n::::n     v:::::v v:::::v      b:::::b     b:::::b]],
      [[  n::::n    n::::n      v:::::v:::::v       b:::::b     b:::::b]],
      [[  n::::n    n::::n       v:::::::::v        b:::::b     b:::::b]],
      [[  n::::n    n::::n        v:::::::v         b:::::bbbbbb::::::b]],
      [[  n::::n    n::::n         v:::::v          b::::::::::::::::b ]],
      [[  n::::n    n::::n          v:::v           b:::::::::::::::b  ]],
      [[  nnnnnn    nnnnnn           vvv            bbbbbbbbbbbbbbbb   ]],
    }
  end

  dashboard.section.header.val = header()

	-- Setup dashboard buttons
  dashboard.section.buttons.val = {
		-- Opens a new unnamed buffer for editning then enters insert mode
    dashboard.button("e", "  New file", ":enew <BAR> startinsert <CR>"),
		-- Opens 'init.lua' file, which lives under $MYVIMRC variable
    dashboard.button("c", "  Configuration directory", ":e $XDG_CONFIG_HOME <CR>"),
		-- Opens 'plugins.lua' file
		dashboard.button("c", "  Plugins", ":e $XDG_CONFIG_HOME/nvim/lua/plugins.lua <CR>"),
		-- Quits Neovim
    dashboard.button("q", "  Quit Neovim", ":qa<CR>"),
  }

  local function footer()
    -- Number of plugins
    local datetime = os.date "%d-%m-%Y  %H:%M:%S"
    local plugins_text = datetime

    -- Quote
    local fortune = require "alpha.fortune"
    local quote = table.concat(fortune(), "\n")

    return plugins_text .. "\n" .. quote
  end

  dashboard.section.footer.val = footer()

  dashboard.section.footer.opts.hl = "Constant"
  dashboard.section.header.opts.hl = "Include"
  dashboard.section.buttons.opts.hl = "Function"
  dashboard.section.buttons.opts.hl_shortcut = "Type"
  dashboard.opts.opts.noautocmd = true

  alpha.setup(dashboard.opts)
end

return M
