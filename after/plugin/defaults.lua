-- defaults.lua
-- This script is loaded after all plugins finished loading
-- thus it's located in /after/plugin/defaults.lua


-- API methods
local api = vim.api
-- Global Neovim variables
local g = vim.g
-- Neovim options which can be set mostly like "set" command in vim
local opt = vim.opt


opt.termguicolors = true -- Enable colors in terminal
opt.hlsearch = true --Set highlight on search
opt.number = true --Make line numbers default
opt.relativenumber = true --Make relative number default
opt.mouse = "a" --Enable mouse mode
opt.breakindent = true --Enable break indent
opt.expandtab = true -- Expand tabs with spaces
opt.undofile = true --Save undo history
opt.ignorecase = true --Case insensitive searching unless /C or capital in search
opt.smartcase = true -- Smart case
opt.updatetime = 250 --Decrease update time
opt.signcolumn = "yes" -- Always show sign column
opt.clipboard = "unnamedplus" -- Access system clipboard
opt.laststatus = 3  -- Global status line

-- More helpful path options
opt.path:remove "/usr/include"
opt.path:append "**"

-- Search files case-insensitive
opt.wildignorecase = true

-- Files ignored in search using :find and similar
opt.wildignore:append "**/node_modules/*"
opt.wildignore:append "**/.git/*"
opt.wildignore:append "**/build/*"
opt.wildignore:append "**/bin/*"
opt.wildignore:append "**/obj/*"

-- Options for Netrw
g.netrw_banner = 0 -- Hide banner
g.netrw_browse_split = 4 -- Open Netrw in previous window
g.netrw_altv = 1 -- Open in the right split
g.netrw_liststyle = 3 -- Tree-style view
g.netrw_list_hide = (vim.fn["netrw_gitignore#Hide"]()) .. [[,\(^\|\s\s)\zs\.\S\+]]  -- Use .gitignore for hiding files

-- Highlight on yank
vim.cmd [[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]]

-- Time in milliseconds to wait for a mapped sequence to complete (for WhichKey)
opt.timeoutlen = 300

-- Vanilla neovim completion options
opt.cot = "menuone"

