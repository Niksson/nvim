-- This is a Lua ftplugin
-- It sets options that are local to buffers written in Lua
vim.bo.shiftwidth = 2
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.textwidth = 120
