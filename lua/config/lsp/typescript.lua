local M = {}

function M.setup(opts)
 local ts = require "typescript"

 local ts_opts = {
   server = {
    enable_import_on_completion = true,
    eslint_bin = "eslint_d",
    eslint_enable_diagnostics = true,
    eslint_enable_disable_comments = true,
   }
 }
 local extended_opts = vim.tbl_deep_extend("force", ts_opts, opts)

 ts.setup(extended_opts)
end

return M
