local M = {}

local ls = require("luasnip")
local ls_extras = require("luasnip.extras")

local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = ls_extras.lambda
local rep = ls_extras.rep
local p = ls_extras.partial
local m = ls_extras.match
local n = ls_extras.nonempty
local dl = ls_extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.expand_conditions")

local function create_snippets()
  ls.add_snippets("all", {
    s("ttt", t "Testing Luasnip")
  })
  ls.add_snippets("lua", {
    ls.parser.parse_snippet("lm", "local M = {}\n\nfunction M.setup()\n $1 \nend\n\nreturn M")
  })
end

function M.setup()

    ls.config.set_config {
    history = true,
    updateevents = "TextChanged,TextChangedI",
    enable_autosnippets = true,
    ext_opts = {
      [types.choiceNode] = {
        active = {
          virt_text = { { "<-", "Error"} },
        }
      }
    }
  }

  require("luasnip.loaders.from_vscode").load()

  create_snippets()
end

return M
