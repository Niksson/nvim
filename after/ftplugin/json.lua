-- This is a JSON ftplugin
-- It sets options that are loacl to buffers written in JSON
vim.bo.shiftwidth = 2
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.textwidth = 120
