local M = {}

local utils = require("utils")
local nls_utils = require("config.lsp.null-ls.utils")
local nls_sources = require("null-ls.sources")

local method = require("null-ls").methods.FORMATTING

M.autoformat = false

function M.toggle()
  M.autoformat = not M.autoformat
  if M.autoformat then
    utils.info("Enabled format on save", "Formatting")
  else
    utils.warn("Disabled format on save", "Formatting")
  end
end

function M.format_on_save()
  if M.autoformat then
    vim.lsp.buf.format({async = false, timeout_ms = 2000})
  end
end

function M.has_formatter(filetype)
  local available = nls_sources.get_available(filetype, method)
  return #available > 0
end

function M.list_registered_providers(filetype)
  local registered_providers = nls_utils.list_registered_providers_names(filetype)
  return registered_providers[method] or {}
end

function M.list_supported_formatters(filetype)
  local supported_formatters = nls_sources.get_supported(filetype, "formatting")
  table.sort(supported_formatters)
  return supported_formatters
end

function M.setup(client, buf)
  local filetype = vim.api.nvim_buf_get_option(buf, "filetype")

  local is_formatting_enabled = false
  if M.has_formatter(filetype) then
    is_formatting_enabled = client.name == "null-ls"
  else
    is_formatting_enabled = not (client.name == "null-ls")
  end

  client.server_capabilities.document_formatting = is_formatting_enabled
  client.server_capabilities.document_range_formatting = is_formatting_enabled

  if is_formatting_enabled then
    vim.api.nvim_create_autocmd("BufWritePre", {
      group = vim.api.nvim_create_augroup("LspFormat", {clear = true}),
      pattern = "<buffer>",
      callback = function ()
        require("config.lsp.null-ls.formatters").format_on_save()
      end
    })
  end

end

return M
