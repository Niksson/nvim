local M = {}

function M.setup()

  require("nvim-tree").setup {

    disable_netrw = true,
    hijack_netrw = true,
    view = {
      number = false,
    },
    filters = {
      custom = {},
    },
    sync_root_with_cwd = true,
    reload_on_bufenter = true,
    respect_buf_cwd = true,
    git = {
      ignore = false,
    },
    update_cwd = true,
    update_focused_file = {
      enable = true,
      update_cwd = true,
    },

  }

end

return M
