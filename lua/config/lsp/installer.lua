local M = {}

function M.ensure_installed(servers_dict)
  local server_names = {}

  for server_name, _ in pairs(servers_dict) do
    table.insert(server_names, server_name)
  end

  local mason_lspconfig = require "mason-lspconfig"
  print(mason_lspconfig)
  mason_lspconfig.setup {
    ensure_installed = server_names,
  }
end

function M.setup(servers, options)
  local server_names = {}
  local server_handlers = {}

  for server_name, server_opts in pairs(servers) do
    local opts = vim.tbl_deep_extend("force", options, server_opts or {})
    table.insert(server_names, server_name)

    server_handlers[server_name] = function()
      require("lspconfig")[server_name].setup(opts)
    end
  end

  local mason_lspconfig = require "mason-lspconfig"
  mason_lspconfig.setup {
    ensure_installed = server_names,
  }
  mason_lspconfig.setup_handlers(server_handlers)
end

return M
