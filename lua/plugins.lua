-- Creates an empty Lua table for storing our plugin module
local M = {}

-- Adds a function called 'setup' to M
function M.setup()
  -- Creates a 'conf' table to store some configuration options for packer
  local conf = {
    display = {
      open_fn = function()
        -- Setups packer to display itself in a floating window with rounded borders
        return require("packer.util").float { border = "rounded" }
      end,
    },
  }

  -- Lazy.nvim bootstrap
  local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
  if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable", -- latest stable release
      lazypath,
    })
  end
  vim.opt.rtp:prepend(lazypath)

  -- Remap leader and local leader to <Space>

  -- First we set Space to Nop
  -- Equivalent to 'noremap <silent> <Space> <Nop>'
  vim.keymap.set("", "<Space>", "<Nop>", { noremap = true, silent = true })
  -- Now we change the actual leader key to be the " " symbol
  vim.g.mapleader = " "
  vim.g.maplocalleader = " "

  -- Plugins
  -- 'use' is a function that accepts a plugin path and options such as configuration function and options for lazy
  -- loading
  local plugins = {
    -- Colorscheme
    -- Here we provide a configuration for a plugin in 'config' field
    {
      "Mofiqul/dracula.nvim",
      config = function()
        require("dracula").setup {
          italic_comment = true,
        }
        --vim.cmd "colorscheme dracula"
      end,
      enabled = false,
    },

    {
      "sainnhe/everforest",
      config = function()
        --vim.cmd "colorscheme everforest"
      end,
      enabled = false,
    },

    {
      "catppuccin/nvim",
      name = "catppuccin",
      config = function()
        require("catppuccin").setup {
          integrations = {
            fidget = true,
            nvimtree = true,
            notify = true,
            gitsigns = true,
            cmp = true,
          },
        }
        vim.cmd "colorscheme catppuccin-macchiato"
      end,
    },

    -- Terminal
    {
      'akinsho/toggleterm.nvim',
      version = '*',
      config = function ()
        require("config.toggleterm").setup()
      end
    },

    -- Markdown preview
    {
      "iamcco/markdown-preview.nvim",
      build = function ()
        vim.fn["mkdp#util#install"]()
      end,
      dependencies = {
        "zhaozg/vim-diagram"
      }
    },

    -- Startup screen
    {
      "goolord/alpha-nvim",
      config = function()
        require("config.alpha").setup()
      end,
    },

    -- Git
    {
      "TimUntersberger/neogit",
      dependencies = {"nvim-lua/plenary.nvim"},
      config = function()
        require("config.neogit").setup()
      end,
      cmd = "Neogit",
    },

    -- nvim-notify
    {
      "rcarriga/nvim-notify",
      event = "VimEnter",
      config = function()
        vim.notify = require "notify"
        vim.notify.setup {
          stages = "fade",
          background_colour = "Normal",
        }
      end,
    },

    -- which-key
    {
      "folke/which-key.nvim",
      config = function()
        require("config.whichkey").setup()
      end,
      event = "VimEnter",
    },

    -- IndentLine
    {
      "lukas-reineke/indent-blankline.nvim",
      event = "BufReadPre",
      config = function()
        require("config.indentblankline").setup()
      end,
    },

    -- Plenary
    {
      "nvim-lua/plenary.nvim",
      module = "plenary",
    },

    -- Web devicons
    {
      "kyazdani42/nvim-web-devicons",
      module = "nvim-web-devicons",
      config = function()
        require("nvim-web-devicons").setup { default = true }
      end,
    },

    -- Better Comment
    {
      "numToStr/Comment.nvim",
      lazy = true,
      keys = { "gc", "gcc", "gbc" },
      config = function()
        require("Comment").setup {}
      end,
    },

    -- Markdown
    {
      "iamcco/markdown-preview.nvim",
      build = function()
        vim.fn["mkdp#util#install"]()
      end,
      ft = "markdown",
      cmd = { "MarkdownPreview" },
    },

    -- Lualine
    {
      "nvim-lualine/lualine.nvim",
      event = "VimEnter",
      config = function()
        require("config.lualine").setup()
      end,
      dependencies = { "kyazdani42/nvim-web-devicons" },
    },

    {
      "ibhagwan/fzf-lua",
      dependencies = { "kyazdani42/nvim-web-devicons" },
    },

    "tpope/vim-vinegar",

    {
      "kyazdani42/nvim-tree.lua",
      dependencies = {
        "kyazdani42/nvim-web-devicons",
      },
      cmd = { "NvimTreeToggle", "NvimTreeClose" },
      config = function()
        require("config.nvimtree").setup()
      end,
    },

    {
      "akinsho/nvim-bufferline.lua",
      event = "BufReadPre",
      dependencies = {
        "kyazdani42/nvim-web-devicons"
      },
      config = function()
        require("config.bufferline").setup()
      end,
    },

    -- Treesitter
    {
      "nvim-treesitter/nvim-treesitter",
      lazy = true,
      event = "BufRead",
      build = ":TSUpdate",
      config = function()
        require("config.treesitter").setup()
      end,
      dependencies = {
         "nvim-treesitter/nvim-treesitter-textobjects",
      },
    },

    {
      "phaazon/hop.nvim",
      cmd = { "HopWord", "HopChar1" },
      config = function()
        require("hop").setup {}
      end,
    },

    {
      "ggandor/lightspeed.nvim",
      keys = { "s", "S", "f", "F", "t", "T" },
      config = function()
        require("lightspeed").setup {}
      end,
    },

    "andymass/vim-matchup",

    "chaoren/vim-wordmotion",

    {
      "L3MON4D3/LuaSnip",
      dependencies = {
        "rafamadriz/friendly-snippets",
      },
      config = function()
        require("config.snip").setup()
      end,
    },

    -- Completion
    {
      "hrsh7th/nvim-cmp",
      event = "InsertEnter",
      lazy = true,
      config = function()
        require("config.cmp").setup()
      end,
      dependencies = {
        "hrsh7th/cmp-buffer",
        "L3MON4D3/LuaSnip",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-nvim-lua",
        "ray-x/cmp-treesitter",
        "hrsh7th/cmp-cmdline",
        "saadparwaiz1/cmp_luasnip",
        "hrsh7th/cmp-calc",
        "f3fora/cmp-spell",
        "hrsh7th/cmp-emoji",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-nvim-lsp-signature-help",
      },
    },

    {
      "windwp/nvim-autopairs",
      dependencies = {
        "nvim-treesitter/nvim-treesitter",
      },
      module = { "nvim-autopairs.completion.cmp", "nvim-autopairs" },
      config = function()
        require("config.autopairs").setup()
      end,
    },

    {
      "windwp/nvim-ts-autotag",
      dependencies = {
        "nvim-treesitter/nvim-treesitter",
      },
      event = "InsertEnter",
      config = function()
        require("nvim-ts-autotag").setup { enable = true }
      end,
    },

    -- LSP
    {
      "neovim/nvim-lspconfig",
      lazy = true,
      event = "BufReadPre",
      config = function()
        require("config.lsp").setup()
      end,
      dependencies = {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "j-hui/fidget.nvim",
        "folke/neodev.nvim",
        "RRethy/vim-illuminate",
        "jose-elias-alvarez/null-ls.nvim",
        "b0o/schemastore.nvim",
        "jose-elias-alvarez/typescript.nvim",
      },
    },

    {
      "williamboman/mason.nvim",
      config = function()
        require("mason").setup()
      end,
    },

    {
      "j-hui/fidget.nvim",
      config = function()
        require("fidget").setup {
          window = {
            blend = 0,
          },
        }
      end,
    },

    -- Telescope
    {
      "nvim-telescope/telescope.nvim",
      lazy = true,
      config = function()
        require("config.telescope").setup()
      end,
      cmd = { "Telescope" },
      module = "telescope",
      keys = { "<leader>f", "<leader>p", "<leader>z", "<leader>l" },
      dependencies = {
        "nvim-telescope/telescope-ui-select.nvim",
        "nvim-lua/popup.nvim",
        "ahmedkhalf/project.nvim",
        "nvim-lua/plenary.nvim",
        "nvim-telescope/telescope-project.nvim",
        "nvim-telescope/telescope-fzf-native.nvim",
        "cljoly/telescope-repo.nvim",
        "nvim-telescope/telescope-file-browser.nvim",
      },
    },

    {
      "ahmedkhalf/project.nvim",
      config = function()
        require("project_nvim").setup {}
      end,
    },

    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },

    -- trouble.nvim
    {
      "folke/trouble.nvim",
      event = "BufReadPre",
      dependencies = {"kyazdani42/nvim-web-devicons"},
      cmd = { "TroubleToggle", "Trouble" },
      config = function()
        require("trouble").setup {
          use_diagnostic_signs = true,
        }
      end,
    },

    -- lspsaga.nvim
    {
      "glepnir/lspsaga.nvim",
      event = "BufReadPost",
      cmd = { "Lspsaga" },
      config = function()
        require("lspsaga").setup {
          ui = {
            colors = require("catppuccin.groups.integrations.lsp_saga").custom_colors(),
            kind = require("catppuccin.groups.integrations.lsp_saga").custom_kind(),
          },
          lightbulb = {
            enable = false,
            enable_in_insert = false,
          },
        }
      end,
    },

    {
      "lewis6991/gitsigns.nvim",
      config = function()
        require("gitsigns").setup()
      end,
    },

    {
      "mfussenegger/nvim-dap",
      lazy = true,
      event = "BufReadPre",
      dependencies = {
        "theHamsta/nvim-dap-virtual-text",
        "rcarriga/nvim-dap-ui",
        "leoluz/nvim-dap-go",
        "jbyuki/one-small-step-for-vimkind",
        "mfussenegger/nvim-dap-python",
        "nvim-telescope/telescope-dap.nvim",
      },
      config = function()
        require("config.dap").setup()
      end,
    },

    { "leoluz/nvim-dap-go" },
    { "jbyuki/one-small-step-for-vimkind" },
  }

  local packer = require "lazy"
  --packer.init(conf)
  packer.setup(plugins)
end

return M
