-- This is a C# ftplugin
-- It sets options that are local to buffers written in C#
vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.softtabstop = 4
vim.bo.textwidth = 120
