local keymap = vim.keymap.set
local default_opts = { silent = true }
local expr_opts = { expr = true, silent = true }

-- Usually considered a better practice to leave insert mode with jk
keymap("i", "jk", "<ESC>", default_opts)
keymap("t", "jk", "<C-\\><C-n>", default_opts)
-- To learn to exit normal move via jk
--keymap("i", "<ESC>", "<nop>", default_opts)

-- Center search results
keymap("n", "n", "nzz", default_opts)
keymap("n", "N", "Nzz", default_opts)

-- Make k and j work as intended in wrapped lines
-- Uses Vimscript instead of Lua function for easier ternary operator
keymap("n", "k", "v:count == 0 ? 'gk' : 'k'", expr_opts)
keymap("n", "j", "v:count == 0 ? 'gj' : 'j'", expr_opts)

-- Indent and then immediately reselect
keymap("v", "<", "<gv", default_opts)
keymap("v", ">", ">gv", default_opts)

-- Paste over currently selected text without ovewriting register
-- "_ is a "black hole" register, so it works like this:
-- 1. Change register to "_
-- 2. Delete visually selected text
-- 3. Paste text via P
keymap("v", "p", '"_dP', default_opts)

-- Switch buffer with vim keys
keymap("n", "<S-h>", ":bprevious<CR>", default_opts)
keymap("n", "<S-l>", ":bnext<CR>", default_opts)

-- Cancel search highlighting with ESC
keymap("n", "<ESC>", ":nohlsearch<Bar>:echo<CR>", default_opts)

-- Move selected line / block of text in visual mode
keymap("x", "K", ":move '<-2<CR>gv-gv", default_opts)
keymap("x", "J", ":move '>+1<CR>gv-gv", default_opts)

-- Resizing panes
keymap("n", "<Left>", ":vertical resize +1<CR>", default_opts)
keymap("n", "<Right>", ":vertical resize -1<CR>", default_opts)
keymap("n", "<Up>", ":resize -1<CR>", default_opts)
keymap("n", "<Down>", ":resize +1<CR>", default_opts)

