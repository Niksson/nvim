local M = {}

function M.setup()
  local toggleterm = require "toggleterm"

  toggleterm.setup {
    open_mapping = [[<c-`>]],
    start_in_insert = true,
    insert_mappings = true,
  }

  vim.api.nvim_create_autocmd('TermOpen', {
    group = vim.api.nvim_create_augroup("ToggleTerm", {clear = true}),
    pattern = "term://*",
    callback = function ()
      set_terminal_keymaps()
    end })
end

function _G.set_terminal_keymaps()
  local opts = {}
  vim.api.nvim_set_keymap('i', '<esc>', [[<C-\><C-n>]], opts)
  vim.api.nvim_set_keymap('i', 'jk', [[<C-\><C-n>]], opts)
  vim.api.nvim_set_keymap('i', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
  vim.api.nvim_set_keymap('i', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
  vim.api.nvim_set_keymap('i', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
  vim.api.nvim_set_keymap('i', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
end

return M
